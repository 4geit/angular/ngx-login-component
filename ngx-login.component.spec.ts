import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxLoginComponent } from './ngx-login.component';

describe('login', () => {
  let component: login;
  let fixture: ComponentFixture<login>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ login ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(login);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
