import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxLoginComponent } from './ngx-login.component';

import { NgxMaterialModule } from '@4geit/ngx-material-module';

@NgModule({
  imports: [
    CommonModule,
    NgxMaterialModule
  ],
  declarations: [
    NgxLoginComponent
  ],
  exports: [
    NgxLoginComponent
  ]
})
export class NgxLoginModule { }
