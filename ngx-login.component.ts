import { Component } from '@angular/core';
import { Router,
         NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { MdSnackBar } from '@angular/material';

import { NgxAuthService } from '@4geit/ngx-auth-service';
import { NgxPageService } from '@4geit/ngx-page-service';

@Component({
  selector: 'ngx-login',
  template: require('pug-loader!./ngx-login.component.pug')(),
  styleUrls: ['./ngx-login.component.scss']
})
export class NgxLoginComponent {

  email: string;
  password: string;
  rememberMe = false;

  constructor(
    private authService: NgxAuthService,
    private pageService: NgxPageService,
    private router: Router,
    private snackBar: MdSnackBar,
  ) { }

  private _checkCredentials() {
    return this.email.length && this.password.length;
  }

  login() {
    if (!this._checkCredentials()) { return; }
    // not loaded yet
    // fix issue https://github.com/angular/angular/issues/6005
    setTimeout(() => {
      this.pageService.isLoaded = false;
    });
    // call api
    this.authService.login({
      email: this.email,
      password: this.password
    }, this.rememberMe)
      .subscribe(
        // succeeded
        res => {
          console.log(res);
          if (this.authService.isLoggedIn) {
            // send message
            this.snackBar.open('You are logged in!', 'Hide', { duration: 2000 });

            // Get the redirect URL from our auth service
            // If no redirect has been set, use the default
            const redirect = this.authService.redirectUrl || '/';

            // Set our navigation extras object
            // that passes on our global query params and fragment
            const navigationExtras: NavigationExtras = {
              queryParamsHandling: 'merge',
              preserveFragment: true
            };

            // Redirect the user
            this.router.navigate([redirect], navigationExtras);
          }
        },
        // failed
        err => {
          // is loaded
          setTimeout(() => {
            this.pageService.isLoaded = true;
          });
          if (err.message) {
            // send error message
            this.snackBar.open(err.message, 'Hide', { duration: 2000 });
          } else {
            // send error message
            this.snackBar.open('The credentials are not correct!', 'Hide', { duration: 2000 });
          }
        },
        () => {
          // is loaded
          setTimeout(() => {
            this.pageService.isLoaded = true;
          });
        }
      )
    ;
  }

}
